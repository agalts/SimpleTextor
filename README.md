### **Простой редактор текста**
#### [Скачать .exe](https://gitlab.com/agalts/SimpleTextor/-/raw/main/TextEditor/TextEditor/bin/Debug/TextEditor.exe?inline=false "### Скачать .exe")

Язык программирования: C# 
Среда разработки: Visual Studio 2016

#### Функционал
**Файл**
- **Новый** - создание нового текстового файла
- **Открыть** - выбрать и открыть уже существующий текстовый файл
- **Сохранить** - сохранение открытого или нового файла

**Правка**
- **Скопировать** - скопировать выделенный текст
- **Вставить** - вставить текст из буфера обмена
- **Вырезать** - скопировать и удалить выделенный текст
- **Отменить** - отменить последнее совершённое действие
- **Повторить** - повторить последнее отменённое действие
- **Выделить всё** - выделяет весь текст
- **Снять выделение** - снимает всё выделение в тексте

